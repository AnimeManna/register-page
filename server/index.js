const express = require('express')
const cors = require('cors')
const server = express();
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const url = 'mongodb://localhost:27017/test'

mongoose.connect(url, {useCreateIndex: true, useNewUrlParser: true});

server.use(cors());
server.use(bodyParser());

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => {

    const usersSchema = new mongoose.Schema({
        email: {
            type: String,
            required: true,
            unique: true,
        },
        password: {
            type: String,
            required: true,
            unique: true,
        },
    })

    const Users = mongoose.model('User', usersSchema);

    server.get('/', (req, res) => {
        res.send('hello world')
    })

    server.post('/register', async (req, res) => {
        const { body } = req
        let response = {}
        const newUser = new Users({...body});
        await newUser.save((err)=>{
            if(err){
                response = {
                    success: false,
                    message:'I know you, maybe you need sign in?'
                }
                res.status(404).send(response);
                return console.error(err)
            }
            response = {
                success: true,
                message: 'Welcome in our family'
            }
            res.status(202).send(response);
        })
    })
});


server.listen(3000, () => {
    console.log('you listening server at 3000 port')
});