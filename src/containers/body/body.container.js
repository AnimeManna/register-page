import React, {Component} from 'react'
import "babel-polyfill";
import FetchProvider from '../../providers/fetch.providers'

export class Body extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {
                email: '',
                password: '',
            },
            message: '',
            success: undefined,
            isValid:false,
        }
        this.handleClick = this.handleClick.bind(this);
        this.changeInputs = this.changeInputs.bind(this);
        this.isValid = this.isValid.bind(this);
    }

    isValid(){
        const {email, password} = this.state.user
        return email.length > 2 && password.length > 2
    }

    async handleClick() {
        const {user} = this.state
        const data = await FetchProvider.createRequest('register', 'post', user)
        this.setState({
            message: data.message,
            success: data.success,
        })
        console.log(this.state);
    }

    changeInputs(e) {
        const {value, name} = e.target;
        const {user} = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value,
            }
        },()=>{
            if(this.isValid()){
                this.setState({
                    isValid:true
                })
            }else{
                this.setState({
                    isValid: false
                })
            }
            })
    }

    render() {
        const {message, isValid} = this.state
        console.log(this.state);
        return (
            <div>
                <input placeholder={'Your email'} type={'text'} onChange={this.changeInputs} name={'email'}/>
                <input placeholder={'Your password'} type={'password'} onChange={this.changeInputs} name={'password'}/>
                <button onClick={this.handleClick} disabled={!isValid} >
                    Click
                </button>
                {message}
            </div>
        )
    }
}