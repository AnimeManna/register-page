import React from 'react'
import ReactDOM from 'react-dom'
import { Body } from "./containers/body/body.container";


ReactDOM.render( <Body /> , document.getElementById('root'))

module.hot.accept();
