const url = 'http://localhost:3000/'

export default class FetchProviders {

    static async createRequest(uri, method, data) {
        const response = await fetch(url + uri, {
            method,
            headers: {
                "Content-Type": "application/json",
            },
            body:JSON.stringify(data)
        })
        const responseData = await response.json();
        return responseData
    }

}